# Crystal Clear for 3DS

# Preparation
1.  Install CFW on your 3DS by following [this guide](https://3ds.hacks.guide/).
2.  Get `Crystal_Clear.gbc` from the [official Discord](https://discordapp.com/invite/xmTXhtV).
3.  Get the latest version of [New Super Ultimate Injector](https://gbatemp.net/threads/discussion-new-super-ultimate-injector-nsui.500376/).
4.  Get `download.zip` from this repository containing `redone_wireless_stuff.patch`, `Icon.png` and `Title.png`.

# Injecting
1. Open **New Super Ultimate Injector**.
2. Press F4 to create a new **Game Boy Color CIA Project**.
3. Navigate to *Project* > *Load ROM...* and upload `Crystal_Clear.gbc`
4. Choose the settings exactly like in the following picture.
5. Navigate to *Project* > *Export CIA...* and download `Pokémon Crystal Clear.cia`.
6. Install `Pokémon Crystal Clear.cia` on your 3DS using **FBI** from the CFW guide.

![alt text](Tutorial.png)

# Have Fun!
You should now be able to play the game!
Please let me know on [reddit](https://www.reddit.com/r/PKMNCrystalClear/comments/e89wzg/crystal_clear_for_3ds/) if you have any issues.